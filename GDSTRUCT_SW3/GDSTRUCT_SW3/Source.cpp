#include<iostream>
#include<string>
#include<conio.h>

using namespace std;

void add(int number, int& sum)
{
	int integers = 0;
	if (number <= 0)
	{
		cout << "\b\b\b = ";
		return;
	}

	if (number > 0)
	{
		integers = integers + number % 10;
		cout << integers << " + ";
		sum = sum + integers;
		number /= 10;
	}
	add(number, sum);
}

void printSum(int& sum)
{
	cout << sum;
}

void fibonacci(int n, int current, int previous)
{
	n--;
	if (n < 0)
	{
		return;
	}

	cout << current << " ";
	int nextTerm = 0;

	nextTerm = current + previous;
	current = previous;
	previous = nextTerm; 

	fibonacci(n, current, previous);
}

bool isPrime(int n, int i = 2)
{
	if (n <= 2)
	{
		return (n == 2) ? true : false;
	}

	if (n % i == 0)
	{
		return false;
	}

	if (i * i > n)
	{
		return true;
	}

	return isPrime(n, i + 1);
}

void main()
{
	int input;
	int sum = 0;
	int choice;
	cout << "Input Number: ";
	cin >> input;
	
	cout << "Computation of Input: ";
	add(input, sum);
	printSum(sum);
	cout << endl;
	
	_getch();
	cout << "Fibonacci series: ";
	int current = 0;
	int previous = 1;
	fibonacci(input, current, previous);
	cout << endl;

	_getch();
	if (isPrime(input))
	{
		cout << "PRIME";
	}

	else
	{
		cout << "NOT PRIME";
	}
	_getch();
}